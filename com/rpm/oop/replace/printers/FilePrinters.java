package com.rpm.oop.replace.printers;

import java.io.FileWriter;
import java.io.IOException;

public class FilePrinters implements IPrinter {
    @Override
    public void print(final String text) {
        try (FileWriter writer = new FileWriter("C:\\Users\\User\\Desktop\\lol1.txt")) {
            writer.write(text);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
