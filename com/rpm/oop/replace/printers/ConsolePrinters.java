package com.rpm.oop.replace.printers;

public class ConsolePrinters implements IPrinter {

    @Override
    public void print(final String text) {
        System.out.println(text);
    }
}
