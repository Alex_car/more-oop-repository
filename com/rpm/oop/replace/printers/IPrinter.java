package com.rpm.oop.replace.printers;

public interface IPrinter {
   void print(final String text);
}