package com.rpm.oop.replace.printers;

public class DecoratePrinter implements IPrinter {

    @Override
    public void print(String text) {
        System.out.println("**********");
        System.out.println(text);
        System.out.println("**********");
    }
}
