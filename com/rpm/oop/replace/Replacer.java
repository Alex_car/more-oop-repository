package com.rpm.oop.replace;

import com.rpm.oop.replace.printers.IPrinter;
import com.rpm.oop.replace.readers.IReader;

import java.io.IOException;

class Replacer {

    private IReader reader;
    private IPrinter printer;

    public Replacer(IReader reader, IPrinter printer) {
        this.reader = reader;
        this.printer = printer;
    }

    void replace() throws IOException {
        final String text = reader.read();
        final String replaceText = text.replaceAll(":c", "c:");
        printer.print(replaceText);
    }
}
