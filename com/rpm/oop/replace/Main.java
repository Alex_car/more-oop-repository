package com.rpm.oop.replace;

import com.rpm.oop.replace.printers.*;
import com.rpm.oop.replace.readers.ConsoleReader;
import com.rpm.oop.replace.readers.FileReader;
import com.rpm.oop.replace.readers.IReader;
import com.rpm.oop.replace.readers.PredefinedReaders;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        /**
         * Меняет грустные смайлики
         * из строки на веселые
         */
        IReader reader = new PredefinedReaders(":c sausage"); //строка
        IPrinter printer = new ConsolePrinters(); //консольный вывод
        Replacer replacer = new Replacer(reader, printer); //получает строку и способ вывода
        replacer.replace(); //выводит строку
        System.out.println();
        /**
        * Меняет грустные смайлики из строки
         * на веселые и выводит длину строки
        */
        IPrinter advPrinter = new AdvConsolePrinter();
        Replacer advReplacer = new Replacer(reader, advPrinter);
        advReplacer.replace();
        System.out.println();
        /**
         * Меняет грустные смайлики из строки
         * на веселые и выводит красивую строку со *
        */
        IPrinter decPrinter = new DecoratePrinter();
        Replacer decReplacer = new Replacer(reader, decPrinter);
        decReplacer.replace();
        System.out.println();
        /**
        * Меняет грустные смайлики из строки
        * на веселые и выводит длину
        */
        IReader iReader = new ConsoleReader(); //ввод через консоль
        Replacer consoleReplacer = new Replacer(iReader, advPrinter);
        consoleReplacer.replace();
        System.out.println();
        /**
        * Меняет грустные смайлики из строки
        * на веселые
        */
        IReader fileReader = new FileReader("C:\\Users\\User\\Desktop\\lol.txt"); //ввод через файл
        Replacer fileReaderReplacer = new Replacer(fileReader, printer);
        fileReaderReplacer.replace();
        System.out.println();
        /**
        * Меняет грустные смайлики из строки
        * на веселы, создает и сохраняет файл
        */
        IPrinter filePrinter = new FilePrinters(); //
        Replacer filePrinterReplacer = new Replacer(reader, filePrinter);
        filePrinterReplacer.replace();
    }
}
