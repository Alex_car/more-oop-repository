package com.rpm.oop.replace.readers;

import java.io.IOException;

public interface IReader {
    String read() throws IOException;
}
