package com.rpm.oop.fileIdentifier;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String localString = "2.5 -5.78 плюс + +67 .8 9. . +. 23.12e+10";
        Pattern fixPattern = Pattern.compile("([+-]?(\\d*\\.\\d+)|(\\d+\\.\\d*))\\W");
        Pattern floatPattern = Pattern.compile("([+-]?((\\d*\\.\\d+)[e][+-](\\d*))|((\\d+\\.\\d*) [e][+-](\\d*)))");

        System.out.print("Введите цифру с какой точкой вывести числа: \n1. Плавающей \n2. Фиксированной \n Цифра: ");
        int numb = scanner.nextInt();

        if (numb > 2 || numb < 0) {
            System.out.println("Ошибка действия");
        } else {
            out(regex(localString, fixPattern, floatPattern, numb));
        }
    }

    public static ArrayList<Pattern> regex(String localString, Pattern fixPattern, Pattern floatPattern, int numb) {
        ArrayList<Pattern> patterns = new ArrayList<>();
        if (numb == 1) {
            Matcher matcher = floatPattern.matcher(localString);
            while (matcher.find()) {
                patterns.add(Pattern.compile(matcher.group()));
            }
        }
        if (numb == 2) {
            Matcher matcher = fixPattern.matcher(localString);
            while (matcher.find()) {
                patterns.add(Pattern.compile(matcher.group()));
            }
        }
        return patterns;
    }

    public static void out(ArrayList<Pattern> patterns) {
        for (Pattern pattern : patterns) {
            System.out.print(pattern + " ");
        }
    }
}
