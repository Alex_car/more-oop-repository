package com.rpm.oop.fileIdentifier.pref;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Prefixes {
    public static void main(String[] args) {
        String s;
        Pattern pattern = Pattern.compile("\\b(пре|при|Пре|При)[а-яё]+\\b");
        ArrayList <String> text= new ArrayList<>();
        Matcher matcher = pattern.matcher("");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\com\\rpm\\oop\\fileIdentifier\\pref\\input.txt"))) {
            while ((s = bufferedReader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                    text.add(matcher.group());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println(text.toString());
        print(text);
    }
    public static void print(ArrayList <String> text) {
        try (FileWriter writer = new FileWriter("src\\com\\rpm\\oop\\fileIdentifier\\pref\\output.txt")) {
            writer.write(String.valueOf(text));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
