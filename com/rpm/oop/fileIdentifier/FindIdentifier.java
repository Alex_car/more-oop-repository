package com.rpm.oop.fileIdentifier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindIdentifier {
    public static void main(String[] args) {
        String listLine = fileReader();
        Pattern pattern = Pattern.compile("[a-zA-Z_][a-zA-Z_0-9]*");
        ArrayList<String> template = find(pattern, listLine);
        out((HashSet<String>) unique(template));
        System.out.println("\n***");
        out(template);
    }

    private static String fileReader() {
        StringBuilder listLine = new StringBuilder();
        try {
            List<String> list = Files.readAllLines(Paths.get("src\\com\\rpm\\oop\\fileIdentifier\\Regex.java"));
            for (String s : list) {
                listLine.append(s); //listLine += s
            }
        } catch (IOException e) {
            System.out.println("not found");
        }
        return listLine.toString();
    }


    private static ArrayList<String> find(Pattern pattern, String listLine) {
        ArrayList<String> patterns = new ArrayList<>();
        Matcher matcher = pattern.matcher(listLine);
        while (matcher.find()) {
            patterns.add(String.valueOf(Pattern.compile(matcher.group())));
        }
        return patterns;
    }

    private static Set<String> unique(ArrayList<String> template) {
        HashSet<String> patterns = new HashSet<>();
        patterns.addAll(template);
        return patterns;
    }

    private static void out(HashSet<String> patterns) {
        for (String pattern : patterns) {
            System.out.print(pattern + " ");
        }
    }

    private static void out(ArrayList<String> patterns) {
        for (String pattern : patterns) {
            System.out.print(pattern + " ");
        }
    }
}

