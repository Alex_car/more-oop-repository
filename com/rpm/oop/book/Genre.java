package com.rpm.oop.book;

public enum Genre {
    ADVENTURE("Приключение"),
    EDUCATIONAL("Научная"),
    FAIRY("Сказка"),
    FANTASY("Фантастика"),
    PROSE("Проза"),
    SCIENCE_FICTION("Научная фантастика");

    private String genre;

    Genre(String genre) {
        this.genre = genre;
    }

    public String getGenre1() {
        return genre;
    }
}
