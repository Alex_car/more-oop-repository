package com.rpm.oop.book;


import java.util.ArrayList;
import java.util.Scanner;

/**
 * реализует работу с книгами
 *
 * @Author IM
 */
public class Main {
    public static void main(String[] args) {
        Book book1 = new Book("Война и мир", "Толстой", Genre.SCIENCE_FICTION, "C:/books/WarAndPiece", 1890);
        Book book2 = new Book("Грокаем алгоритмы", "Бхаргава", Genre.SCIENCE_FICTION, "C:/books/GrokhaemAlgorithms", 2010);
        Book book3 = new Book("Колобок", "Народная", Genre.FAIRY, "C:/books/GingerbreadMan", 1851);
        Book book4 = new Book("книга4", "Толстой", Genre.PROSE, "C:/books/WarAnPiece", 1890);
        Book book5 = new Book("книга5", "автор5", Genre.EDUCATIONAL, "C:/books/WarAdPiece", 1890);
        Book book6 = new Book("Колобок", "автор6", Genre.FANTASY, "C:/books/WarndPiece", 1899);
        Book book7 = new Book("книга7", "Толстой", Genre.FANTASY, "C:/books/WaAndPiece", 1790);
        Book book8 = new Book("книга8", "Бхаргава", Genre.ADVENTURE, "C:/books/WrAndPiece", 890);
        Book book9 = new Book("книга9", "Толстой", Genre.FANTASY, "C:/books/arAndPiece", 1880);
        Book book10 = new Book("книга10", "Толстой", Genre.ADVENTURE, "C:/books/WarAndPiec", 2090);
        Book[] books = {book1, book2, book3, book4, book5, book6, book7, book8, book9, book10};
        System.out.println("сортировка по автору");
        surnameSort(books);
        out(books);

        System.out.println("\nСортировка по году");
        yearSort(books);
        out(books);

        System.out.println("\nПоиск по названию");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите название: ");
        String name = scanner.nextLine();
        isEmpty(findName(books, name));

        System.out.println("\nФильтр по автору");
        System.out.print("Введите автора: ");
        String author = scanner.nextLine();
        isEmpty(findAuthor(books, author));

        System.out.println("\nФильтр по жанру");
        System.out.print("Введите жанр: Приключение, Научная, Фантастика, Проза, Научная фантастика");
        String genre = scanner.nextLine();
        isEmpty(findGenre(books, genre));


    }

    /**проверят наполненность массива
     *
     * @param array ArrayList эл-в
     */
    private static void isEmpty(ArrayList<Book> array) {
        if (array.isEmpty()) {
            System.out.println("Пусто");
        } else {
            sout(array);
        }
    }

    /**
     * выводит массив эл-в
     *
     * @param array массив
     */
    private static void out(Book[] array) {
        for (Book book : array) {
            System.out.println(book + " ");
        }
    }

    /**
     * сортирует массив по фамилии автора
     *
     * @param array массив
     */
    private static void surnameSort(Book[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                int isCompare = array[i].getAuthor().compareToIgnoreCase(array[j].getAuthor());
                if (isCompare > 0) {
                    Book temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
    }

    /**
     * сортирует массив по году издания
     *
     * @param array массив
     */
    private static void yearSort(Book[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i].getPublishYear() < array[j].getPublishYear()) {
                    Book temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
    }

    /**
     * находит книги по названию
     *
     * @param array массив
     * @param name  название
     * @return массив с заданным названием книг
     */
    private static ArrayList<Book> findName(Book[] array, String name) {
        ArrayList<Book> findBook = new ArrayList<>();

        for (Book book : array) {
            if (name.equalsIgnoreCase(book.getTitle())) {
                findBook.add(book);
            }
        }
        return findBook;
    }

    /**
     * находит книги по жанру
     *
     * @param array массив
     * @param genre жанр
     * @return массив с заданным жанром книг
     */
    private static ArrayList<Book> findGenre(Book[] array, String genre) {
        ArrayList<Book> findGenre = new ArrayList<>();
/*
        Genre localGenre = Genre.NOVEL;

        if (genre.equalsIgnoreCase("Сказка")) {
            localGenre = Genre.FAIRY;
        }
        if (genre.equalsIgnoreCase("Трагедия")) {
            localGenre = Genre.TRAGEDY;
        }
        if (genre.equalsIgnoreCase("Образовательная")) {
            localGenre = Genre.SCIENTIFIC;
        }
*/
        for (Book book : array) {
            if (genre.equals(book.getGenre().getGenre1())) {
                findGenre.add(book);
            }
        }
        return findGenre;
    }

    /**
     * находит книги по автору
     *
     * @param array  массив
     * @param author автор
     * @return массив с заданным автором книг
     */
    private static ArrayList<Book> findAuthor(Book[] array, String author) {
        ArrayList<Book> findAuthor = new ArrayList<>();

        for (Book book : array) {
            if (author.equalsIgnoreCase(book.getAuthor())) {
                findAuthor.add(book);
            }
        }
        return findAuthor;
    }

    /**
     * выводит ArrayList эл-в
     *
     * @param arrayList массив
     */
    private static void sout(ArrayList<Book> arrayList) {
        for (Book book : arrayList) {
            System.out.println(book);
        }
    }
}
