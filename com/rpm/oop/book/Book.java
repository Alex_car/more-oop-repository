package com.rpm.oop.book;

public class Book {
    private String title;
    private String author;
    private Genre genre;
    private String url;
    private int publishYear;

    public Book(String title, String author, Genre genre, String url, int publishYear) {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.url = url;
        this.publishYear = publishYear;
    }

    public Book() {
        this("Ода", "Пушкин", Genre.FANTASY, "url",1895);
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public Genre getGenre() {
        return genre;
    }

    public String getUrl() {
        return url;
    }

    public int getPublishYear() {
        return publishYear;
    }

    @Override
    public String toString() {
        return "Книга{" +
                "Название='" + title + '\'' +
                ", Автор='" + author + '\'' +
                ", Жанр=" + genre.getGenre1() +
                ", URL='" + url + '\'' +
                ", Год издания=" + publishYear +
                '}';
    }
}
